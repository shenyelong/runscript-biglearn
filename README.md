# README #

Example 1 : BigLearn for DNN

Run : mono BigLearn.DeepNet.exe dnn.config

In dnn.config
"

BUILDER	DNN

TRAIN	train.tsv

VALID	valid.tsv

TEST	test.tsv

GPUID	-2	-2:SSE; -1:CPU; 0-7:GPU

FEATURE-DIM	1587

MINI-BATCH	128

IS-SPARSE-FEA	1	0:dense;1:sparse

LOSS-FUNCTION	0	{ BinaryClassification = 0, Regression = 1, MultiClassification = 6, OrdinalLogisticRegression = 4 }

EVALUATION	0	{ AUC = 0, NDCG = 1, MSE = 2, ACCURACY = 3, MAP = 4, ORDINALACCURACY = 5, ORDEDLOGITACCURACY = 6, PLAINOUTPUT = 7, PERPLEXITY = 8, 

LAYER-DIM	1024,1
ACTIVATION	2,0
LAYER-BIAS	1,1
LAYER-DROPOUT	0.2,0

GAMMA	1
SCORE-PATH	tmp.score
METRIC-PATH	tmp.metric
MODEL-PATH	DNN-Model

LEARN-RATE	0.01
ITERATION	100
OPTIMIZER	1