if ! [ -n "$6" ]; then
	echo "extract no label training data" 
	"$(dirname "$0")/"OfflineFeatureExtraction.exe PairTextFeatureExtractor -l3gVocab "$(dirname "$0")/"l3g.txt -maxSeq $2 -maxNum $2 -input $1 -batchSize 1024 -srcBin $4 -tgtBin $5 -filter 1 -isHeader 0 -shuffle $3 -srcIdx 0 -tgtIdx 1 -scoreIdx -1
fi

if [ -n "$6" ]; then
  echo "extract label training data" 
  "$(dirname "$0")/"OfflineFeatureExtraction.exe PairTextFeatureExtractor -l3gVocab "$(dirname "$0")/"l3g.txt -maxSeq $2 -maxNum $2 -input $1 -batchSize 1024 -srcBin $4 -tgtBin $5 -scoreBin $6 -filter 0 -isHeader 0 -shuffle $3 -srcIdx 0 -tgtIdx 1 -scoreIdx 2 
fi

#SequenceLabelingFeatureExtractor -input $1.sort -batchSize 1024 -isHeader 0 -output $2 -version 2

