import os;
import sys;
import codecs;

# Read source/target sequence file.
inputSequenceFileName = sys.argv[1]; 

# UTF code : 8 or 16;
utfID = (sys.argv[2]);

# read src vocab file.
srcVocabFileName = sys.argv[3];

# read tgt vocab file.
tgtVocabFileName = sys.argv[4];

# write src/tgt word idx file.
outputIdxFileName = sys.argv[5];

srcUFilter = int(sys.argv[6]);
tgtUFilter = int(sys.argv[7]);


def LoadVocab(mfile, mvocab):
	global utfID;
	mf = codecs.open(mfile,'r', 'utf-'+utfID);
	for line in mf:
		items = line.strip().split('\t');
		mvocab[items[0]] = [int(items[2]),int(items[1])];
		
	mf.close();

def PullVocab(item, vocab):
	if(vocab.has_key(item)):
		return vocab[item][1];
	else:
		return -1;
		

def ExtractIdx(text, vocab, filter):
	items = text.strip().replace('\t',' ').split(' ');
	result = "";
	len = 0;
	
	bgnId = PullVocab('<#begin#>', vocab);
	if(bgnId >= 0):
		result = str(bgnId);
		len = len + 1;
	for item in items:
		tID = PullVocab(item.strip(), vocab);
		if(tID >= 0):
			result = result + " " + str(tID);
			len = len + 1;
		elif(filter > 0):
			result = "";
			len = 0;
			break;
	
	endId = PullVocab('<#end#>', vocab);
	if(len > 0 and endId >= 0):
		result = result + " " + str(endId);
		len = len + 1;
	return [result.strip(), len];
	
def ExtractPairIndex(input, srcVocab, tgtVocab, output, srcUFilter, tgtUFilter):
	global utfID;
	sVocab = {};
	tVocab = {};
	LoadVocab(srcVocab, sVocab);
	LoadVocab(tgtVocab, tVocab);
	print "#srcVocab", len(sVocab);
	print "#tgtVocab", len(tVocab);
	print "#filter src", srcUFilter;
	print "#filter tgt", tgtUFilter;
	pairLen = 0;
	maxSrcLen = 0;
	maxTgtLen = 0;
	mf = codecs.open(input,'r', 'utf-'+utfID);
	wFile = file(output,'w');
	for line in mf:
		items = line.strip().split('\t');
		srcIdxs = ExtractIdx(items[0], sVocab, srcUFilter);
		tgtIdxs = ExtractIdx(items[1], tVocab, tgtUFilter);
		
		if(srcIdxs[1] == 0 or tgtIdxs[1] == 0):
			continue;
		else:
			wFile.write(srcIdxs[0]+"\t"+tgtIdxs[0]+"\n");
			pairLen += 1;
		if(srcIdxs[1] > maxSrcLen):
			maxSrcLen = srcIdxs[1];
		
		if(tgtIdxs[1] > maxTgtLen):
			maxTgtLen = tgtIdxs[1];
	wFile.close();
	mf.close();
	print "#total pair len", pairLen;
	print "#max src len ", maxSrcLen;
	print "#max tgt len ", maxTgtLen;
	

ExtractPairIndex(inputSequenceFileName, srcVocabFileName, tgtVocabFileName, outputIdxFileName, srcUFilter, tgtUFilter);
#ReadSrcTgt('data\, tgtName, wSrcFile, wTgtFile);