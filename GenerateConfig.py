import os;
import sys;


def LoadConfigureFile(filePath):
	ConfigDict = {};
	fPath = file(filePath);
	for line in fPath:
		items = line.strip().split('\t');
		if(len(items) >= 2):
			mkey = items[0];
			mvalue = items[1];
			ConfigDict[mkey] = mvalue;
	fPath.close();
	return ConfigDict;

def SaveConfigureFile(filePath, dict):
	fPath = file(filePath,'w');
	for mkey in dict:
		fPath.write(mkey+'\t'+dict[mkey]+'\n');
	fPath.close();
# Load Default configure file.
argLen = len(sys.argv);
configPath = sys.argv[1];
dateString = sys.argv[argLen - 1];
tmpConfigPath = configPath + '.' + dateString;

ConfigDict = LoadConfigureFile(sys.argv[1]);

num = (argLen - 2) / 2
for argNum in range(0, num):
	ConfigDict[sys.argv[2 + 2 * argNum]] = sys.argv[3 + 2 * argNum];
print ConfigDict;
SaveConfigureFile(tmpConfigPath, ConfigDict);