#cd "$(dirname "$0")"
python "$(dirname "$0")/"reformat.py $1 $1.tmp
sort -t $'\t' -nk 1,1 -nk 2,2 $1.tmp > $1.sort
"$(dirname "$0")"/OfflineFeatureExtraction.exe SequenceLabelingFeatureExtractor -input $1.sort -batchSize $3 -isHeader 0 -output $2 -FeaNum $4 -version 3 -isCleanZeros $5
rm $1.tmp
rm $1.sort
