import os;
import sys;
import codecs;

# Read word sequence file.
wSequenceFileName = sys.argv[1]; 

# UTF code : 8 or 16;
utfID = (sys.argv[2]);

# read word vocab file.
wVocabFileName = sys.argv[3];

# write word idx file.
wIdxFIleName = sys.argv[4];



tVocab = {};

def LoadVocab(mfile, mvocab):
	mf = codecs.open(mfile,'r', 'utf-'+utfID);
	for line in mf:
		items = line.strip().split('\t');
		if(len(items) == 2):
			mvocab[items[0]] = [1, int(items[1])];
		else:
			mvocab[items[0]] = [int(items[2]),int(items[1])];
	mf.close();

def pullVocab(item, vocab):
	if(vocab.has_key(item)):
		return vocab[item][1];
	else:
		return -1;

# Append <#begin#> in vocab.
isAppendBegin = 0;

# Append <#end#> in vocab.
isAppendEnd = 0;

LoadVocab(wVocabFileName, tVocab);
if(pullVocab('<#begin#>', tVocab) >= 0):
	isAppendBegin = 1;

if(pullVocab('<#end#>', tVocab) >= 0):
	isAppendEnd = 1;

if(len(sys.argv) >= 7):
	isAppendBegin = int(sys.argv[5]);
	isAppendEnd = int(sys.argv[6]);

print "VocabSize ", len(tVocab);
sys.stdout.flush();

tFile = codecs.open(wSequenceFileName,'r', 'utf-'+utfID);
wFile = file(wIdxFIleName,'w');
tgtMaxLen = 0;
for line in tFile:
	items = line.strip().replace('\t',' ').split(' ');
	tgt = "";
	mlen = 0;
	if(isAppendBegin):
		tgt = str(pullVocab('<#begin#>', tVocab));
		mlen += 1;
	for t in items:
		tID = pullVocab(t.strip(), tVocab);
		if(tID >= 0):
			tgt = tgt+" " + str(tID);
			mlen += 1;
		else:
			tgt = tgt + " " + str(1);
			mlen += 1;
	if(isAppendEnd):
		tgt = tgt+" " + str(pullVocab('<#end#>', tVocab));
		mlen += 1;
	wFile.write(tgt.strip()+"\n");
	if(mlen > tgtMaxLen):
		tgtMaxLen = mlen;
tFile.close();
wFile.close();

print "maximum sequence Length", tgtMaxLen;


#ReadSrcTgt('data\, tgtName, wSrcFile, wTgtFile);