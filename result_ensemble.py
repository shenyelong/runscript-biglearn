## result.ensemble
import os;
import sys;
from collections import Counter;
import operator

fea = [];
truth = [];
pred = [];
numF = len(sys.argv);

entityVocab = {};
vocab = file(sys.argv[1]);
for line in vocab:
	items = line.strip().split('\t');
	name = items[0];
	if(name.startswith('@entity')):
		idx = int(items[1]);
		entityVocab[idx] = name;
vocab.close();

def vocabLook(lstr):
	global entityVocab;
	if(lstr.startswith('@entity')):
		return lstr;
	elif(not entityVocab.has_key(int(lstr)-1)):
		print int(lstr);
		return "@placehold";
	else:
		return entityVocab[int(lstr)-1];
		
for i in range(2, numF):
	print sys.argv[i];
	rowIdx = 0;
	mfile = file(sys.argv[i]);
	for line in mfile:
		items = line.strip().split('\t');
		l = vocabLook(items[0]);
		f = items[1].strip().split(' ');
		feaDict = {};
		for mf in f:
			kv = mf.split(':')
			key = vocabLook(kv[0]);
			if(not feaDict.has_key(key)):
				feaDict[key] = 0;
			feaDict[key] += float(kv[1]);
		p = max(feaDict.iteritems(), key=operator.itemgetter(1))[0]
		
		if(i == 2):
			truth.append(l);
			pred.append([]);
			fea.append({});
			
		for k in feaDict:
			if(fea[rowIdx].has_key(k)):
				fea[rowIdx][k] += feaDict[k];
			else:
				fea[rowIdx][k] = feaDict[k];
		pred[rowIdx].append(p);
		rowIdx += 1;
	mfile.close();
	RowNum = rowIdx;

voteLabel = 0;
voteScore = 0;
for i in range(0, RowNum):
	lid = max(k for k,v in Counter(pred[i]).items() if v>=1)
	if(lid == truth[i]):
		voteLabel += 1;
	sid = max(fea[i].iteritems(), key=operator.itemgetter(1))[0]
	if(sid == truth[i]):
		voteScore += 1;
print "vote label accuracy", voteLabel, voteLabel * 1.0 / RowNum;
print "vote score accuracy", voteScore, voteScore * 1.0 / RowNum;

	
	
	
	
