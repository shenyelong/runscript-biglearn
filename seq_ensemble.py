#Sequence Ensemble

import os;
import sys;
import math;

num_ensembles = (len(sys.argv) - 2) / 2;

ensemble_results = [];

ensemble_file = file(sys.argv[len(sys.argv)-1],'w');

for i in range(0, num_ensembles):
	
	fi = file(sys.argv[1 + 2 * i]);
	w = float(sys.argv[2 + 2 * i]);
	
	line_idx = 0;
	for line in fi:
		items = line.strip().split('\t');
		
		if(line_idx >= len(ensemble_results)):
			ensemble_results.append({});
		
		num_pairs = len(items) / 2;
		for k in range(0, num_pairs):
			mkey = items[2 * k];
			mvalue = math.exp(float(items[2 * k + 1]));
			
			if(ensemble_results[line_idx].has_key(mkey)):
				ensemble_results[line_idx][mkey] += mvalue * w;
			else:
				ensemble_results[line_idx][mkey] = mvalue * w;
			
		line_idx += 1;
	fi.close();
	

for mdict in ensemble_results:
	result = "";
	for key, value in sorted(mdict.iteritems(), key=lambda (k,v): (v,k)):
		result = str(key) +"\t" + str(value) + "\t" + result;
	ensemble_file.write(result.strip()+"\n");
ensemble_file.close();
