import os;
import sys;
import codecs;

### Extract the Vocab as Index.

# Read word sequence file.
wSequenceFileName = sys.argv[1]; 

# Append <#begin#> in vocab.
isAppendBegin = int(sys.argv[2]);

# Append <#end#> in vocab.
isAppendEnd = int(sys.argv[3]);

# UTF code : 8 or 16;
utfID = (sys.argv[4]);

# Write word vocab file.
wVocabFileName = sys.argv[5];

vocabThreshold = 0;
if(len(sys.argv) >= 7):
	vocabThreshold = int(sys.argv[6]);

eleVocab = {};
def pushVocab(item, vocab):
	if(vocab.has_key(item)):
		vocab[item][0] += 1;
	else:
		vocab[item] = [1, len(vocab)];
	return vocab[item][1];

def RefineVocab(mvocab, filterNum):
	newVocab = {};
	newVocab['<#end#>'] = [mvocab['<#end#>'][0], 0];
	for mkey in mvocab:
		if(mvocab[mkey][0] >= filterNum):
			if(mvocab[mkey][1] == 0):
				continue;
			newVocab[mkey] = [mvocab[mkey][0], len(newVocab)];
	return newVocab;
	
def saveVocab(fileStr, vocab):
	global vocabThreshold;
	mf = codecs.open(fileStr,'w','utf-'+utfID);
	wordIdx = 0;
	for word in vocab:
		if(vocab[word][0] < vocabThreshold):
			continue;
		mf.write(word+"\t"+str(wordIdx)+"\t"+str(vocab[word][0])+"\n");
		wordIdx += 1;
	mf.close();

def IsEnglish(mstr):
	for c in mstr:
		if( (ord(c) >= ord('a') and ord(c) <= ord('z')) or (ord(c) >= ord('A') and ord(c) <= ord('Z'))):
			return True;
	return False;

	

sFile = codecs.open(wSequenceFileName,'r', 'utf-'+utfID);
lineNum = 0;
for line in sFile:
	items = line.strip().replace('\t',' ').split(' ');
	#print line[0],line[1], line,items;
	#break;
	if(isAppendBegin > 0):
		pushVocab('<#begin#>', eleVocab);
	
	src = "";
	for s in items:
		if(s.strip() == '' or s.strip() == ' '):
			continue;
		pushVocab(s.strip(), eleVocab);
	
	if(isAppendEnd > 0):
		pushVocab('<#end#>', eleVocab);

sFile.close();


#eleVocab = RefineVocab(eleVocab, 500);
saveVocab(wVocabFileName, eleVocab);
