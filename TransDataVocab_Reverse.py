import os;
import sys;
import codecs;

# Read source sequence file.
inputFileName = sys.argv[1]; 

# UTF code : 8 or 16;
utfID = (sys.argv[2]);

# read src vocab file.
vocabFileName = sys.argv[3];


# write src/tgt word idx file.
outputFileName = sys.argv[4];

ignoreS = {};
if(len(sys.argv) > 5):
	iStr = sys.argv[5].split(',');
	for ms in iStr:
		ignoreS[int(ms)] = 1;

def LoadVocab(mfile, mvocab):
	global utfID;
	mf = codecs.open(mfile,'r', 'utf-'+utfID);
	for line in mf:
		items = line.strip().split('\t');
		mvocab[int(items[1])] = items[0];
	mf.close();

def PullVocab(item, vocab):
	if(vocab.has_key(item)):
		return vocab[item];
	else:
		return "Unknown";
		

def ExtractSeq(ids, vocab):
	items = ids.strip().split(' ');
	result = "";
	global ignoreS;
	for item in items:
		tID = int(item);
		if(ignoreS.has_key(tID)):
			continue;
		item = PullVocab(tID, vocab);
		if(item == '<#begin#>'):
			continue;
		if(item == '<#end#>'):
			continue;
		result = result + " " + item;
	return result.strip();
	
def ExtractWord(input, vocab, output):
	global utfID;
	Vocab = {};
	LoadVocab(vocab, Vocab);
	print "#Vocab", len(Vocab);
	mf = codecs.open(input,'r', 'utf-'+utfID);
	wFile = file(output,'w');
	for line in mf:
		items = line.strip().split('\t');
		results = ExtractSeq(items[0], Vocab);
		#print results;
		#results.encode('utf-8',errors='replace');
		results = results.encode('ascii',errors='ignore');
		wFile.write(results+"\n");
	wFile.close();
	mf.close();
	

ExtractWord(inputFileName, vocabFileName, outputFileName);
