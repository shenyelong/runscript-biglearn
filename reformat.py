import os;
import sys;

f1 = file(sys.argv[1]);
f2 = file(sys.argv[2],'w');
instanceDict = {};
for line in f1:
	items = line.strip().split('\t');
	header = items[0].strip().split(':');
	if(len(header) != 3):
		print line;
	instances = header[2].strip().split('-');
	if(len(instances) != 2):
		print line;
	id = instances[0];
	order = instances[1];
	instanceDict[instances[0]] = 1;
	f2.write(id+"\t"+order+"\t"+line.strip()+"\n");
f1.close();
print len(instanceDict);
f2.close();