@echo off
Setlocal EnableDelayedExpansion
SET TF="%VS120COMNTOOLS%\..\IDE\tf.exe"
SET CMD_ROOT=%~dp0

for /f "delims=" %%a in ('date /T') do set date=%%a

for /f "delims=" %%a in ('time /T') do set time=%%a

echo =============================================================
echo Run ScopeMLTest on %date% %time%
echo =============================================================

set argc=0
for %%x in (%*) do (
set /a argc+=1
)

:start
if %argc%==1 (
if "%1"=="-h" (
    echo USAGE:   %~n0 [Lable]
    echo EXAMPLE 1: %~n0 ScopeML_Dec_Release
    exit /B 1
)

set QueryLabel=%1
set Label=L%1
echo Test specified label: ^[!Label!^]
) else (
set Label=T
echo Test the latest version^!^! 
)

set ScopeMLIntBetaWorkSpace=ScopeMLIntBetaWorkSpace
echo Try to clear tempory workspace ...
call %TF% workspaces %ScopeMLIntBetaWorkSpace% 2>nul 1> nul
IF !errorlevel!==0 call echo Y|%TF% workspace /delete %ScopeMLIntBetaWorkSpace% 2>nul 1> nul

echo Try to create tempory workspace
call %TF% workspace /new %ScopeMLIntBetaWorkSpace% /collection:http://vstfcodebox:8080/tfs/Delta/ /noprompt /location:local 
IF NOT !errorlevel!==0 (
echo Failed create temp working space and sync new code
GOTO END
)

echo Log version information ...
%TF% history $/biglearn /version:%Label% /noprompt /recursive /stopafter:1 > Versions.txt 

IF NOT %Label%==T (
echo .>>Versions.txt
%TF% labels %QueryLabel% >> Versions.txt )

:: Get version information to veriables
for /f "tokens=1-3* skip=2" %%a in (Versions.txt) do (
set ChangeSet=%%a
set User=%%b
set Date=%%c
set Comments=%%d
)

set VersionInfo=!ChangeSet!_!User!
set ScopeMLWorkingRoot=%CMD_ROOT%ScopeMLTest_!VersionInfo!

SET MSBUILD="%VS120COMNTOOLS%\..\IDE\devenv.com"
SET ScopeMLBuildDir="%ScopeMLWorkingRoot%\biglearn\ScopeML\ScopeMLE2ETest\bin\x64\Release"
SET RootDir=%ScopeMLWorkingRoot%\biglearn\ScopeML\
SET SCOPEML_DIRECTORY=%RootDir%ScopeMLScript\Module
SET SCOPE_DIRECTORY=%CMD_ROOT%runtime

if EXIST "%ScopeMLWorkingRoot%" (
echo This version %ChangeSet% already synced.
::GOTO BUILDSOLUTION
GOTO END
)

mkdir "%ScopeMLWorkingRoot%"

echo Get latest version of code to tempory directory ...
call %TF% workfold /map $/biglearn "%ScopeMLWorkingRoot%\biglearn"
call %TF% get $/biglearn /all /version:%Label% /recursive

:BUILDSOLUTION
echo Clear tempory workspace
call echo Y|%TF% workspace /delete %ScopeMLIntBetaWorkSpace% 2>nul 1>nul

echo .
:: Build solution
call %MSBUILD% "%ScopeMLWorkingRoot%\biglearn\ScopeML\ScopeML.sln" /build "Release|x64" /out build_log.txt
if NOT !errorlevel!==0  GOTO REPORT_BUILDERROR

COPY /Y %RootDir%\ScopeMLE2ETest\bin\x64\Release\ScopeMLE2ETest.exe %CMD_ROOT%\ScopeMLE2ETest.exe
COPY /Y %RootDir%\ScopeMLE2ETest\BuildModule.bat %CMD_ROOT%\BuildModule.bat
COPY /Y %RootDir%\ScopeMLE2ETest\UploadBetaVersion.bat %CMD_ROOT%\UploadBetaVersion.bat
COPY /Y %RootDir%\ScopeMLE2ETest\SyncAndBuildLatestVersion.bat %CMD_ROOT%\SyncAndBuildLatestVersion.bat

SET BuildName=Beta
call BuildModule.bat %SCOPE_DIRECTORY% %ScopeMLBuildDir% %SCOPEML_DIRECTORY% %BuildName% %ChangeSet% %User% 
if NOT !errorlevel!==0  GOTO END

:UPLOADBETA
for /f "tokens=1* delims= " %%a in ('date /T') do set today=%%a
for /f "tokens=1-4 delims=/ " %%w in ('date /T') do set yymmdd=%%z_%%x_%%y
::if NOT "!today!" EQU "Mon" GOTO END
call UploadBetaVersion.bat %SCOPE_DIRECTORY% %SCOPEML_DIRECTORY% %BuildName% %ChangeSet% %User% 

echo ==================DONE====================== 

del VersionHistory.txt
GOTO END

:END
call %TF% workspaces %ScopeMLIntBetaWorkSpace% 2>nul 1>nul
IF !errorlevel!==0 call echo Y|%TF% workspace /delete %ScopeMLIntBetaWorkSpace% 2>nul 1>nul

echo =============================================================
echo Done
echo =============================================================
exit /B 0

:REPORT_BUILDERROR
call ScopeMLE2ETest.exe rpt -v %VersionInfo% -l build_log.txt -u %User% -m "Failed to build ScopeML solution." 
GOTO END
