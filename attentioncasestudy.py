##attention case study
import sys;
import os;

### source idx file.
srcFile = sys.argv[1];

### source vocab file.
srcVocabFile = sys.argv[2];

### target vocab file.
tgtVocabFile = sys.argv[3];

### case study file.
caseFile = sys.argv[4];

### read me file.
readmeFile = file(sys.argv[5],'w');

def loadVocab(mfile):
	mf = file(mfile);
	mvocab = {};
	for line in mf:
		items = line.strip().split('\t');
		id = int(items[1]);
		mvocab[id] = items[0];
	mf.close();
	return mvocab;

srcVocab = loadVocab(srcVocabFile);
tgtVocab = loadVocab(tgtVocabFile);

sFile = file(srcFile);
cFile = file(caseFile);

for cline in cFile:
	items = cline.strip().split('\t');
	tgts = items[0].strip().split(' ');
	attentions = items[1].strip().split(' ');
	
	srcString = "";
	srcText = sFile.readline().strip().split(' ');
	for idx in range(0, len(srcText)):
		srcString = str(idx + 1)+":::"+ srcVocab[int(srcText[idx])];
	
	readmeFile.write(srcString+"\n");
	for idx in range(1, len(tgts)):
		tgt = tgtVocab[int(tgts[idx])];
		attStr = "";
		if(idx == 1):
			attStr = "";
		else:
			attStr = attentions[idx-1];
		
		readmeFile.write(tgt+"\t\t"+attStr+"\n\n\n");
cFile.close();
sFile.close();
readmeFile.close();