Exception Message:
E_COSMOS_USER_BADSUPERVERTEXARGS: Bad or missing parameters passed to run super vertex
Description:
To run super vertex you need to pass super vertex name and at least one input and output stream.
Example: "ScopeRuntime.exe -scopeVertex Extract_SV1 -i Input1 -o output"

Stack Trace:
   at ScopeRuntime.Runner.RunSuperVertex(String[] args, String inputOutputDirectory, String tempDir, Boolean outputHeader)
   at ScopeRuntime.Sandboxer.RunInSandbox(String[] args, Dictionary`2 param)

DIAGNOSTIC INFORMATION:

	ROW CONTENTS:

